<?php

namespace App\Http\Controllers;

use App\Models\PersonModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function index()
    {
        $persons = PersonModel::all();
        return view('index', compact('persons'));
    }

    public function store(Request $request)
    {
        $input = $request->input('data');

        // Remove all string and found number
        preg_match('/^(.*?)(\d+)(.*)$/', $input, $matches);

        // Hasil mapping
        // Grup 1 ((.*?)): "CUT MINI "
        // Grup 2 ((\d+)): "28"
        // Grup 3 (.*): " BANDA ACEH"

        $age = (int) ($matches[2] ?? 0);
        $name = isset($matches[1]) ? trim($matches[1]) : '';
        $city = isset($matches[3]) ? trim($matches[3]) : '';

        // Remove Tahunm. thn, th
        $city = preg_replace('/\b(TAHUN|thn|th)\b/i', '', $city);

        $user = PersonModel::create([
            'name' => strtoupper($name),
            'age' => $age,
            'city' => strtoupper($city),
        ]);

        if ($user) {
            return Redirect::route('home')->with('success', 'Successfully save data!');
        } else {
            return Redirect::route('home')->with('error', 'Something went wrong!');
        }
    }
}
