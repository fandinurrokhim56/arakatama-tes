<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>Arkatama Test</title>
</head>

<body>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif


    <div class="w-50 mx-auto mt-5">
        <div class="form-container">
            <form action="/user/store" method="post">
                @csrf
                <div class="mb-3">
                    <label for="input" class="form-label">Input Your data:</label>
                    <input type="text" name="data" class="form-control" placeholder="CUT MINI 28 BANDA ACEH"
                        required>
                </div>

                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>

        <div class=" mt-5">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>City</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($persons as $index => $person)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $person->name }}</td>
                            <td>{{ $person->age }}</td>
                            <td>{{ $person->city }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
